import json
import unittest
import requests


class TestSwagger(unittest.TestCase):

    def test_request_get_response_positive(self):
        # Send a request to the API server and store the response.
        response = requests.get('http://localhost:5000/v1/products')
        # Confirm that the request-response cycle completed successfully.
        self.assertEqual(response.status_code, 200)

    def test_request_get_response_negative(self):
        # Send a request to the API server and store the response.
        response = requests.get('http://localhost:5000/v1/products')
        # Confirm that the request-response cycle completed successfully.
        self.assertEqual(response.status_code, 400)

    def test_request_post_response_positive(self):
        # Send a request to the API server and store the response.
        response = requests.get('http://localhost:5000/v1/product')
        # Confirm that the request-response cycle completed successfully.
        self.assertEqual(response.status_code, 200)

    def test_request_post_response_negative(self):
        # Send a request to the API server and store the response.
        response = requests.get('http://localhost:5000/v1/product')
        # Confirm that the request-response cycle completed successfully.
        self.assertEqual(response.status_code, 400)

    def test_get_request_positive(self):
        response = requests.get('http://localhost:5000/v1/products')
        json_response = json.loads(response.text)
        self.assertEqual(len(json_response), 6)
        self.assertEqual(response.status_code, 200)

    def test_get_request_negative(self):
        response = requests.get('http://localhost:5000/v1/products')
        #json_response = json.loads(response.text)
        #self.assertEqual(len(json_response), 4)
        self.assertEqual(response.status_code, 400)

    def test_post_request_values1_positive(self):
        response = requests.post('http://localhost:5000/v1/product', 'Personalised cufflinks', '45.00')
        json_response = json.loads(response.text)
        self.assertEqual(json_response, 'null')
        self.assertEqual(response.status_code, 200)

    def test_post_request_values1_negative(self):
        response = requests.post('http://localhost:5000/v1/product', 'Personalised cufflinks', '45.00')
        #json_response = json.loads(response.text)
        #self.assertEqual(json_response, 'null')
        self.assertEqual(response.status_code, 400)

    def test_post_request_values2_positive(self):
        response = requests.post('http://localhost:5000/v1/product', 'Lavender heart', '9.25')
        json_response = json.loads(response.text)
        self.assertEqual(json_response, 'null')
        self.assertEqual(response.status_code, 200)

    def test_post_request_values2_negative(self):
        response = requests.post('http://localhost:5000/v1/product', 'Lavender heart', '9.25')
        #json_response = json.loads(response.text)
        #self.assertEqual(json_response, 'null')
        self.assertEqual(response.status_code, 400)

    def test_delete_request_values1_positive(self):
        response = requests.delete('http://localhost:5000/v1/product/1', product_code=1)
        self.assertEqual(response.status_code, 200)

    def test_delete_request_values1_negative(self):
        response = requests.delete('http://localhost:5000/v1/product/1', product_code=1)
        self.assertEqual(response.status_code, 200)

    def test_delete_request_values2_positive(self):
        response = requests.delete('http://localhost:5000/v1/product/2', product_code=2)
        self.assertEqual(response.status_code, 200)

    def test_delete_request_values2_negative(self):
        response = requests.delete('http://localhost:5000/v1/product/2', product_code=2)
        self.assertEqual(response.status_code, 200)

    def test_put_request_value_positive(self):
        response = requests.put('http://localhost:5000/v1/product/3', 'name=Heart&price=100')
        self.assertEqual(response.status_code, 200)

    def test_put_request_value_negative(self):
        response = requests.put('http://localhost:5000/v1/product/3', 'name=Heart&price=100')
        self.assertEqual(response.status_code, 400)

    def test_get_request_with_product_code_positive(self):
        response = requests.get('http://localhost:5000/v1/product/1')
        json_response = json.loads(response.text)
        self.assertEqual(len(json_response), 6)
        self.assertEqual(response.status_code, 200)

    def test_get_request_with_product_code_negative(self):
        response = requests.get('http://localhost:5000/v1/product/1')
        #json_response = json.loads(response.text)
        #self.assertEqual(len(json_response), 6)
        self.assertEqual(response.status_code, 400)

    def test_get_request_with_product_code_values2_positive(self):
        response = requests.get('http://localhost:5000/v1/product/2')
        json_response = json.loads(response.text)
        self.assertEqual(len(json_response), 6)
        self.assertEqual(response.status_code, 200)

    def test_get_request_with_product_code_values2_negative(self):
        response = requests.get('http://localhost:5000/v1/product/2')
        #json_response = json.loads(response.text)
        #self.assertEqual(len(json_response), 6)
        self.assertEqual(response.status_code, 400)

    def test_accessing_product_code_using_post_gives_a_failure(self):
        response = requests.post('http://localhost:5000/v1/product/2')
        self.assertEqual(response.status_code, 400)
